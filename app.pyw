""" GUI for real time plotting using automatic yaml settings"""

import yaml
from appJar import gui
import numpy as np
import threading
import time
import os
import tempfile
import datetime
import glob
import traceback

SETTINGS_FILE = 'app_settings.yaml'


class YamlSettings(object):

    def __init__(self, settings_path):

        self.settings_path = settings_path
        if os.path.isfile(self.settings_path):
            with open(self.settings_path) as stream:
                self.settings = yaml.safe_load(stream)
        else:
            self.settings = dict()
            with open(self.settings_path, 'w') as stream:
                yaml.safe_dump(self.settings, stream)

    def get(self, item):
        try:
            return self.settings[item]
        except KeyError:
            return None

    def set(self, key, value):
        self.settings[key] = value
        with open(self.settings_path, 'w') as stream:
            yaml.safe_dump(self.settings, stream)


class PlotRefreshThread(threading.Thread):
    def __init__(self, *argv):
        super(PlotRefreshThread, self).__init__(*argv)
        self._stop_event = threading.Event()
        self._clear_event = threading.Event()

    def run(self):
        global fig1, fig2, app_settings

        out_dir = app_settings.get('Output Directory')
        if out_dir == '':
            temp_dir = tempfile.TemporaryDirectory()
            out_dir = temp_dir.name
            using_tempdir = True
        else:
            temp_dir = None
            using_tempdir = False
            if not os.path.exists(out_dir):
                os.makedirs(out_dir, exist_ok=True)

        while not self.stopped():
            print("Working ....")
            try:
                if self._clear_event.is_set():
                    # TODO: Perform clear actions
                    self._clear_event.clear()

                process_data(out_dir, figure=fig1)
                process_data(out_dir, figure=fig2)
                app.queueFunction(refresh_plots)
            except Exception:
                traceback.print_exc()
                self._clear_event.set()
                app.queueFunction(press_start_stop)
                app.queueFunction(show_popup, traceback.format_exc(), error=True)

            time.sleep(5)

        if using_tempdir:
            temp_dir.cleanup()

    def stop(self):
        self._stop_event.set()

    def clear_cached(self):
        self._clear_event.set()

    def stopped(self):
        return self._stop_event.is_set()


def press_start_stop():
    global app, processor_thread
    if app.getButton('Start') == 'Start':
        app.setButton("Start", "Stop")
        press_update(None, clear_thread_cache=False)
        processor_thread = PlotRefreshThread()
        processor_thread.start()
    elif app.getButton('Start') == 'Stop':
        app.setButton("Start", "Start")
        processor_thread.stop()


def process_data(out_dir, figure):
    global app_settings

    figure.clear()

    ax1 = figure.add_subplot(111)
    ax1.plot(np.random.randn(30))


def refresh_plots():
    global app
    app.refreshPlot("plot1")
    app.refreshPlot("plot2")


def show_popup(text, error=False):
    if error:
        app.errorBox('Error', text)
    else:
        app.infoBox('Info', text)


def exit_check():
    global processor_thread
    if processor_thread is not None:
        processor_thread.stop()
    print("Exiting...")
    return True


def list_file_info():
    show_popup('File Info')


def press_browse(button):
    global app, app_settings
    if button == 'Browse 1':
        filetypes = [('Any', '*')]
        text_entry = "File 1"
    elif button == 'Browse 2':
        filetypes = [('Yaml', '*.yaml')]
        text_entry = "File 2"
    elif button == 'Browse 3':
        filetypes = [('Yaml', '*.yaml')]
        text_entry = "File 3"
    elif button == 'Browse 4':
        filetypes = [('Text', '*.txt')]
        text_entry = "File 4"
    else:
        return

    filename = app.openBox("Open File", fileTypes=filetypes)
    if len(filename) > 0:
        file_path = os.path.abspath(filename)
        app.setEntry(text_entry, file_path)
        save_ui_settings()


def press_update(button, clear_thread_cache=True):
    global processor_thread

    changed_cached = False
    for spinbox in ["Val 1", "Val 2"]:
        if app_settings.get(spinbox) != int(app.getSpinBox(spinbox)):
            changed_cached = True

    save_ui_settings()

    if clear_thread_cache and changed_cached and processor_thread is not None:
        if processor_thread.isAlive():
            processor_thread.clear_cached()
            print("Thread Cache Cleared!")


def save_ui_settings():
    global app, app_settings
    for text_entry in ["File 1", "File 2", "File 3", "Output Directory", "File 4"]:
        app_settings.set(text_entry, app.getEntry(text_entry))
    for spinbox in ["Val 1", "Val 2", "Val 3", "Val 4", "File Index"]:
        app_settings.set(spinbox, int(app.getSpinBox(spinbox)))


def load_ui_settings():
    global app, app_settings
    for text_entry in ["File 1", "File 2", "File 3", "Output Directory", "File 4"]:
        if app_settings.get(text_entry) is not None:
            app.setEntry(text_entry, app_settings.get(text_entry))
    for spinbox in ["Val 1", "Val 2", "Val 3", "Val 4", "File Index"]:
        if app_settings.get(spinbox) is not None:
            app.setSpinBox(spinbox, str(app_settings.get(spinbox)))


if __name__ == '__main__':
    app_settings = YamlSettings(SETTINGS_FILE)
    processor_thread = None
    with gui("Real Time Processing") as app:
        app.setStopFunction(exit_check)
        app.setBg("white", tint=True)

        app.addLabelEntry("File 1", row=0, column=0, colspan=2)
        app.addButton("Browse 1", press_browse, row=0, column=2, colspan=1)
        app.setButton("Browse 1", "Browse")
        app.addButton("List File 1", list_file_info, row=0, column=3, colspan=1)

        app.addLabelEntry("File 2", row=1, column=0, colspan=2)
        app.addButton("Browse 2", press_browse, row=1, column=2, colspan=1)
        app.setButton("Browse 2", "Browse")

        app.addLabelEntry("File 3", row=2, column=0, colspan=2)
        app.addButton("Browse 3", press_browse, row=2, column=2, colspan=1)
        app.setButton("Browse 3", "Browse")

        app.addLabelEntry("File 4", row=3, column=0, colspan=2)
        app.addButton("Browse 4", press_browse, row=3, column=2, colspan=1)
        app.setButton("Browse 4", "Browse")

        app.addButton("List File 4", list_file_info, row=3, column=3, colspan=1)
        app.addLabelSpinBoxRange("File Index", 0, 100, row=3, column=4, colspan=1)

        app.addLabelEntry("Output Directory", row=4, column=0, colspan=2)

        app.buttons(["Start", "Exit"], [press_start_stop, app.stop], row=5)

        app.startTabbedFrame("TabbedFrame")
        app.startTab("Tab1")
        fig1 = app.addPlotFig("plot1", row=6, width=12, height=8, colspan=5)

        app.addLabelSpinBoxRange("Val 1", 1, 20, row=7, column=0, colspan=1)
        app.addLabelSpinBoxRange("Val 2", 1, 50, row=7, column=1, colspan=1)
        app.addLabelSpinBoxRange("Val 3", 1, 20, row=7, column=2, colspan=1)
        app.addLabelSpinBoxRange("Val 4", 1, 50, row=7, column=3, colspan=1)
        app.button("Update", press_update, row=7, column=4, colspan=1)
        app.stopTab()

        app.startTab("Tab2")
        fig2 = app.addPlotFig("plot2", row=6, width=12, height=8, colspan=5)
        app.stopTab()

        load_ui_settings()
